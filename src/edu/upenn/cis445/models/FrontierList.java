package edu.upenn.cis445.models;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;

/**
 * @author Jatin Sharma
 *
 */

public class FrontierList<T> {

	private int size;
	private int maxAllowedSize = 25000;
	private ArrayList<LinkedList<T>> buckets;
	private int nextBucketNumber;
	private int numBuckets = 500;
	private BloomFilter frontierCache = new BloomFilter(200000,3,Hash.MURMUR_HASH);
	
	// constructor
	public FrontierList(){
		this.size = 0;
		this.buckets = new ArrayList<LinkedList<T>>(numBuckets);
		this.nextBucketNumber = 0;
		
		// initialize all 10 buckets
		for (int i = 0; i < numBuckets; i++) {
			this.buckets.add(new LinkedList<T>());
		}
	}

	// O(1) 
	public synchronized boolean add(T object) {
		boolean status = false;
		if(!frontierCache.membershipTest(new Key(object.toString().getBytes()))){
			// pick a bucket
			Random rand = new Random();
			int bucketNumber = rand.nextInt(numBuckets);
			
			// add to the randomly chosen bucket
			LinkedList<T> bucket = (LinkedList<T>) this.buckets.get(bucketNumber);
			if(bucket == null){
				bucket = new LinkedList<T>();
			}
			// adds at the end of the list
			status = bucket.add(object);
			frontierCache.add(new Key(object.toString().getBytes()));
			if(status)
				this.size++;
		}
		return status;
	}
	
	// O(lengthOfInput) 
	public synchronized void addAll(Collection<T> collection) {
		for(T object : collection){
			add(object);
		}
	}

	// O(1) amortized
	public synchronized T get() {
		
		// if there is no nextElement yet generate it
		LinkedList<T> bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
		if (bucket == null || this.size == 0) {
			return null;
		}else{
			while(bucket.size() == 0){
				// bucket is empty so go for next bucket
				nextBucketNumber = (nextBucketNumber+1)%numBuckets;
				bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
			}
			// finally we have a non-empty bucket return first element w/o removing it
			return bucket.get(0);
		}

	}

	// O(1)
	public synchronized boolean isEmpty() {
		if(this.size == 0)
			return true;
		else
			return false;
	}
	
	// O(1) amortized
	public synchronized T remove() {
		
		T object = get();
		if(object != null){
			LinkedList<T> bucket = ((LinkedList<T>)this.buckets.get(nextBucketNumber));
			nextBucketNumber = (nextBucketNumber+1)%numBuckets;
			bucket.remove(0);
			this.size--;
			return object;
		}else{
			return null;
		}
	}

	// O(1)
	public int size() {
		return this.size;
	}
	
	// O(numBuckets) = O(10) = O(1) 
	public synchronized void clear() {
		this.size = 0;
		this.buckets = new ArrayList<LinkedList<T>>(numBuckets);
		this.nextBucketNumber = 0;
		
		// initialize all 10 buckets
		for (int i = 0; i < numBuckets; i++) {
			this.buckets.add(new LinkedList<T>());
		}
		// to collected dangling references to original buckets
		System.gc();
	}
	
	// O(sizeOfFrontierList) 
	public synchronized File writeToFile(String prefix, String suffix) throws IOException {

		File fout = File.createTempFile(prefix, suffix);
		FileOutputStream fos;
		BufferedWriter bw = null;
		try {
			fos = new FileOutputStream(fout);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for(LinkedList<T> bucket : this.buckets){
				for(T object : bucket){
					bw.write(object.toString());
					bw.newLine();
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fout;
	}
	
	public static void main(String[] args){
		
		FrontierList<String> frontier = new FrontierList<String>();
		
		System.out.println(frontier.size());
		
		frontier.add("1");
		frontier.add("2");
		frontier.add("3");
		
		List<String> objectList = new ArrayList<String>();
		objectList.add("4");
		objectList.add("5");
		frontier.addAll(objectList);
		
//		frontier.writeToFile("models/frontierListDemo.txt");
		
		//1
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//2
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//3
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		frontier.clear();
		
		//4
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//5
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//6
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		
	}

}