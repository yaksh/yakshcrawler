package edu.upenn.cis445.models;

public class UrlResponse {
	
	private String url;
	private String content;
	
	public UrlResponse(String url, String content) {
		this.url = url;
		this.content = content;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
