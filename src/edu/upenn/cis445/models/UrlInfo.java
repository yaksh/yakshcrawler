package edu.upenn.cis445.models;

/**
 * @author cis455
 * protocol = http
	authority = example.com:80
	host = example.com
	port = 80
	path = /docs/books/tutorial/index.html
	query = name=networking
	filename = /docs/books/tutorial/index.html?name=networking
 */
public class UrlInfo {
	
	private String protocol;
	private String hostAndPort;
	private String host;
	private String path;
	private String query;
	private String fileName;
	
	public synchronized String getProtocol() {
		return protocol;
	}
	public synchronized void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public synchronized String getHostAndPort() {
		return hostAndPort;
	}
	public synchronized void setHostAndPort(String hostAndPort) {
		this.hostAndPort = hostAndPort;
	}
	public synchronized String getHost() {
		return host;
	}
	public synchronized void setHost(String host) {
		this.host = host;
	}
	public synchronized String getPath() {
		return path;
	}
	public synchronized void setPath(String path) {
		this.path = path;
	}
	public synchronized String getQuery() {
		return query;
	}
	public synchronized void setQuery(String query) {
		this.query = query;
	}
	public synchronized String getFileName() {
		return fileName;
	}
	public synchronized void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
