package edu.upenn.cis445.models;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Set;

/**
 * @author Jatin Sharma
 *
 */

public class SpreadFrontierList<T> {

	private int size;
	private HashMap<String, LinkedList<T>> buckets;
	private LinkedList<T> nextBucket;
	private String nextDomain;
	
	// constructor
	public SpreadFrontierList(){
		this.size = 0;
		this.buckets = new HashMap<String, LinkedList<T>>();
		this.nextDomain = "";
		this.nextBucket = null;
	}

	// O(1) 
	public boolean add(T object) {
		
		// get domain
		String domain;
		try {
			domain = new URL(object.toString()).getHost();
			LinkedList<T> bucket;
			
			// if the domain already exists as a bucket insert into it
			if(this.buckets.containsKey(domain)){
				bucket = (LinkedList<T>) this.buckets.get(domain);
			}else{
				// else create a new bucket and add in there
				bucket = new LinkedList<T>();
				this.buckets.put(domain, bucket);
			}
			
			// add to the retrieved bucket
			boolean status = bucket.add(object);
			if(status)
				this.size++;
			return status;
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}
	
	// O(lengthOfInput) 
	public void addAll(Collection<T> collection) {
		for(T object : collection){
			add(object);
		}
	}

	// O(1) amortized
	public T get() {
		
		Random rand = new Random();
		String[] domains = this.buckets.keySet().toArray(new String[buckets.keySet().size()]);
		int index = rand.nextInt(domains.length);
				
		if (this.buckets == null || this.size == 0) {
			return null;
		}else{
			// if there is no nextBucket or bucket is empty find first non-empty bucket
			while(this.nextBucket == null || nextBucket.size() == 0){
				this.buckets.remove(this.nextDomain);
				this.nextDomain = domains[index];
				this.nextBucket = this.buckets.get(this.nextDomain);
			}

			// finally we have a non-empty bucket return first element w/o removing it
			return this.nextBucket.get(0);
		}
	}
	
	// O(1) amortized
	public T remove() {
		
		T object = get();
		if(object != null){
			this.nextBucket.remove(0);
			this.size--;
			return object;
		}else{
			return null;
		}
	}

	// O(1)
	public boolean isEmpty() {
		if(this.size == 0)
			return true;
		else
			return false;
	}
	
	public void print(){
		for(String key : this.buckets.keySet()){
			System.out.println("Seed: " + key);
		}
	}
	
	

	// O(1)
	public int size() {
		return this.size;
	}
	
	// O(numBuckets)
	public void clear() {
		this.size = 0;
		this.buckets = new HashMap<String, LinkedList<T>>();
		this.nextDomain = "";
		this.nextBucket = null;
		
		// to collected dangling references to original buckets
		System.gc();
	}
	
	// O(numOfElements) needs to be optimized to O(1) using bloomFilter or HashMap
	public boolean contains(T object){
		
		
		String domain = "";
		try {
			domain = new URL(object.toString()).getHost();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LinkedList<T> bucket;
		// if the domain already exists as a bucket insert into it
		if(this.buckets.containsKey(domain)){
			bucket = this.buckets.get(domain);
			if(bucket.contains(object))
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	// O(sizeOfFrontierList) 
	public File writeToFile(String prefix, String suffix) throws Exception {

		File fout = File.createTempFile(prefix, suffix);
		fout.deleteOnExit();
		FileOutputStream fos;
		BufferedWriter bw = null;
		try {
			fos = new FileOutputStream(fout);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for(String domain : this.buckets.keySet()){
				LinkedList<T> bucket = this.buckets.get(domain);
				for(T object : bucket){
					bw.write(object.toString());
					bw.newLine();
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return fout;
	}
	
	public static void main(String[] args) throws Exception{
		
		SpreadFrontierList<String> frontier = new SpreadFrontierList<String>();
		
		System.out.println(frontier.size());
		
		frontier.add("http://www.alexa.com/topsites/global");
		frontier.add("https://www.quora.com/");
		frontier.add("http://www.w3schools.com/");
		
		List<String> objectList = new ArrayList<String>();
		objectList.add("http://www.alexa.com/siteinfo/youtube.com");
		objectList.add("http://www.alexa.com/siteinfo/facebook.com");
		frontier.addAll(objectList);
		
		frontier.writeToFile("models/frontierListDemo", ".txt");
		
		for(String domain : frontier.buckets.keySet()){
			LinkedList<String> urlList = frontier.buckets.get(domain);
			System.out.println("(domain: "+domain +", size: "+urlList.size()+")");
		}
		
		//1
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//2
		System.out.println(frontier.size());
		System.out.println(frontier.get());
		System.out.println(frontier.remove());
		//3
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
//		frontier.clear();
		
		//4
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//5
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		//6
		System.out.println(frontier.size());
		System.out.println(frontier.remove());
		
	}

}
