package edu.upenn.cis455.job;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.conf.*; 
import org.apache.hadoop.io.*; 
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import edu.upenn.cis455.storage.S3Storage;
import org.apache.hadoop.mapreduce.Reducer;
 
public class CrawlerReducer extends Reducer<Text, LongWritable, Text ,Text>{

	private String cacheKey;
	private BloomFilter localCache = new BloomFilter();
	private S3Storage s3;
	
    public void setup(Context context){
    	Configuration config = context.getConfiguration();
    	cacheKey = config.get("localCache");
    }
	
	public void reduce(Text key, Iterator<LongWritable> value, Context context) throws IOException {
		try{
			s3 = new S3Storage(CrawlerMain.awsAccessKey, CrawlerMain.awsSecretKey);
//			setupCache(); || !crawledAlready(key.toString())
			if(true ){
				String id = UUID.randomUUID().toString();
				context.write(new Text(id), key);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}		
	}
	
//	public void reduce(Text key, Iterable<LongWritable> value, Context context){
//		try{
//			s3 = new S3Storage(CrawlerMain.awsAccessKey, CrawlerMain.awsSecretKey);
//			setupCache();
//			if(!crawledAlready(key.toString())){
//				String id = UUID.randomUUID().toString();
//				context.write(new Text(id), key);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//	}
	
	private boolean crawledAlready(String url){
		return localCache.membershipTest(new Key(url.getBytes()));
	}
	
	private void setupCache(){
		try{
			File f = new File("Cache");
			f.deleteOnExit();
			s3.download("yaksh-caching", cacheKey, f);
			DataInput in = new DataInputStream(new FileInputStream(f));
			localCache.readFields(in);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
