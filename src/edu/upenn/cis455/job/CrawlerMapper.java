package edu.upenn.cis455.job;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.*; 
import org.apache.hadoop.io.*; 
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.google.common.collect.Lists;
import edu.upenn.cis445.models.Node;
import edu.upenn.cis445.models.PageDigest;
import edu.upenn.cis445.models.PageGraph;
import edu.upenn.cis445.models.Response;
import edu.upenn.cis445.models.UrlInfo;
import edu.upenn.cis455.crawler.ClientRequest;
import edu.upenn.cis455.crawler.RobotsParser;
import edu.upenn.cis455.storage.DynamoStorage;
import edu.upenn.cis455.storage.S3Storage;
import org.apache.hadoop.mapreduce.Mapper;

public class CrawlerMapper extends Mapper<Text, Text, Text, LongWritable> {
	
	Log log = LogFactory.getLog(CrawlerMapper.class);
	private final static LongWritable one = new LongWritable(1);
	private Text word = new Text();
	private CopyOnWriteArrayList<Node> URLFrontier = new CopyOnWriteArrayList<>();
	private BloomFilter localCache = new BloomFilter(200000,3,Hash.MURMUR_HASH);
	private RobotsParser parsedRobots = new RobotsParser();
	private LinkedList<Node> extractedLinks = new LinkedList<>();
	private List<String> links = new ArrayList<>();
	private HashMap<String, PageDigest> preFetched = new HashMap<>();
	private long pageSizeLimit = 5 * 1024 * 1024;
	private List<PageGraph> linkGraph = new ArrayList<>();
	private List<PageDigest> storeVisitedUrls = new ArrayList<>();
	private List<File> filesToUpload = new ArrayList<>();
	private File fileDir;
	private String cacheKey;
	private String docBucket = "search-documents";
	

    public void setup(Context context){
    	Configuration config = context.getConfiguration();
    	cacheKey = config.get("localCache");
    }
	

	public void map(Text key, Text value,Context context)
			throws IOException {
	try{
		log.info("**********Crawling started (Map Phase)*************");
		System.out.println("**********Crawling started (Map Phase)*************");
			DynamoStorage db = new DynamoStorage(CrawlerMain.awsAccessKey, CrawlerMain.awsSecretKey);
			ClientRequest req = new ClientRequest();
			S3Storage s3 = new S3Storage(CrawlerMain.awsAccessKey, CrawlerMain.awsSecretKey);
			
			String[] urls = value.toString().split(",");
			for(String url : urls){
				Node node = new Node(url, 1);
				URLFrontier.add(node);
//					links.add(url);
			}
//				for(List<String> link : Lists.partition(links, 100)){
//					List<Item> r = db.getUrls("page-digest", "url", link);
//					for(Item item : r){
//	    				PageDigest page = new PageDigest();
//	    				page.setContentType(item.get("contentType").toString());
//	    				page.setUrl(item.get("url").toString());
//	    				page.setLastCrawled(item.getLong("lastCrawled"));
//	    				preFetched.put(item.get("url").toString(), page);
//	    			}
//				}
			int crawlCount = 0;
			while(true){
				try{
					if (URLFrontier.size() > 0) {
						if (URLFrontier.get(0).getDepth() > 4)
							break;
					} else {
						break;
					}
					Node node = getLinkUrlFrontier();
					if (node == null)
						break;
					if (!allowedToCrawl(node.getUrl())) {
						System.out.println("Not allowed to crawl (robots.txt)"
								+ node.getUrl());
						continue;
					}
					if (isUrlVisited(node.getUrl())) {
						System.out.println("Crawler visited url "
								+ node.getUrl());
						continue;
					}
					long lastCrawled = 0;
					// if(preFetched.containsKey(node.getUrl())){
					// lastCrawled =
					// preFetched.get(node.getUrl()).getLastCrawled();
					// }
					log.info("Crawling: " + node.getUrl());
					Response response = req
							.sendHead(node.getUrl(), lastCrawled);
					if (canFetchBody(response, node.getUrl())) {
						crawlCount++;
						Response res = req.sendGet(node.getUrl());
						processNewDoc(res, node);
						queueFilesUpload(node, res);
					}
					addToLocalCache(node.getUrl());
					if(node.getDepth() == 4)
						context.write(new Text(node.getUrl()), one);
					for (List<PageDigest> pages : Lists.partition(storeVisitedUrls, 100)) {
						db.storeUrls(pages);
					}
					for (List<PageGraph> links : Lists.partition(linkGraph, 100)) {
						db.storeGraph(links);
					}
					s3.upload("yaksh-caching", cacheKey, saveCache());
					MultipleFileUpload upload = S3Storage.transferManager
							.uploadFileList(docBucket, "", fileDir,
									filesToUpload);
					upload.waitForCompletion();
					cleanUp();
					System.out.println("*********Finished Crawling (Map phase) Crawled pages = "
									+ crawlCount);
					log.info("*********Finished Crawling (Map phase) Crawled pages = "+ crawlCount);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * @return
	 * Fetch links from the queue of links that are getting appended
	 */
	private Node getLinkUrlFrontier(){
		if(URLFrontier.size() > 0 ){
			return URLFrontier.remove(0);
		}
		return null;
	}
	
	/**
	 * @param url
	 * @return
	 * Decides whether we can proceed with the given url based 
	 * on the robot.txt
	 */
	public boolean allowedToCrawl(String url){
		try{
			return parsedRobots.canICrawl(url);
		}
		catch(Exception e){
			return false;
		}
	}
	
	/**
	 * @param url
	 * @return
	 * Checks to see whether the given url was
	 * crawled previosly or not
	 */
	private boolean isUrlVisited(String url){
		return localCache.membershipTest(new Key(url.getBytes()));
	}
	
	private void addToLocalCache(String url){
		localCache.add(new Key(url.getBytes()));
	}
	
	private boolean canFetchBody(Response res, String url){
		boolean result = true;
		try{
			if(res.getResponseCode() == 200){
				if(res.getContentLength() > pageSizeLimit){
					result = false;
					System.out.println("No GET request due to content limit restrictions for " + url);
				}
				else
					result = true;
			}
			else{
				if(res.getResponseCode() == 304){
					System.out.println("Url " + url + " unmodified since last crawl");
				}
				result = false;
			}
		}
		catch(Exception e){
			result = false;
		}		
		return result;
	}
	
	/**
	 * @param req
	 * Processes the new document to parse the content type
	 * time and body of the new document
	 */
	private void processNewDoc(Response res, Node node){
		if(node.getDepth() == 1){
			PageDigest page = new PageDigest();
			page.setUrl(node.getUrl());
			page.setId(UUID.randomUUID().toString());
			page.setContentType(res.getContentType());
			page.setLastCrawled(System.currentTimeMillis());
			storeVisitedUrls.add(page);
		}
		if(!res.getContentType().equals("") && res.getContentType().contains("html")){
			Document d = null;
			try{
				if(!res.getContent().isEmpty()){
					d = Jsoup.parse(res.getContent());
				}
			}
			catch(Exception e){
				System.err.println("Error parsing html body");
				e.printStackTrace();
				return;
			}
			if(d != null){
				fetchLinksFromDoc(d, node);
			}
		}
	}
	
	/**
	 * @param doc
	 * @param url
	 * Fetch all the links fron a given url
	 */
	private void fetchLinksFromDoc(Document doc, Node node){
		Elements links = doc.getElementsByTag("a");
		Elements links1 = doc.getElementsByTag("link");
		if(links1.size() > 0)
			links.addAll(links1);
		String[] linkStore = new String[links.size()];
		int count  = 0;
		for (int i = 0; i < links.size(); i++) {
			String href = links.get(i).attr("href");
			if(href != null && !href.equals("")){
				count++;
				String l = getAbsoluteURL(node.getUrl(), href);
				linkStore[i] = l;
				if (!URLFrontier.contains(l) && !isUrlVisited(l) && allowedToCrawl(l)){
					Node n = new Node(l, (node.getDepth() + 1));
					URLFrontier.add(n);
				}
			}
		}
		if(linkStore.length > 0){
			PageGraph linkGraphNode = new PageGraph();
			linkGraphNode.setId(UUID.randomUUID().toString());
			linkGraphNode.setUrl(node.getUrl());
			linkGraphNode.setNumOutboundLinks(count);
			Set<String> s = linkGraphNode.getOutboundLinks();
			if(s == null){
				s = new HashSet();
			}
			for(String link : linkStore){
				s.add(link);
			}
			linkGraphNode.setOutboundLinks(s);
			linkGraph.add(linkGraphNode);
		}
	}
	
	public String getAbsoluteURL(String url, String link) {
		String absUrl = url;
		UrlInfo info = ClientRequest.getUrlInfo(url);
		String pro = info.getProtocol();
		String host = info.getHostAndPort();
		String result = "";
		if(link.startsWith("http://") || link.startsWith("https://")){
			result = link;
		}
		else{
			if(link.startsWith("//")){
				link = link.substring(1);
			}
			if(!link.startsWith("/")){
				link = "/" + link;
			}
			result = pro + "://" + host + link;
		}
		return result;
	}
	
	private void queueFilesUpload(Node node, Response res){
		Writer writer = null;
		try{
			fileDir = new File("TempStore");
			if(!fileDir.exists()){
				fileDir.mkdir();
			}
			File f1 = File.createTempFile(node.getUrl(), ".txt", fileDir);
			f1.deleteOnExit();
			writer = new OutputStreamWriter(new FileOutputStream(f1));
			writer.write(node.getUrl() + "\t");
			writer.write(res.getContent().replaceAll("\n", ""));
	        filesToUpload.add(f1);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if(writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private File saveCache() throws IOException{
		File file = new File("TempStore/caching");
		file.deleteOnExit();
		if(!file.exists())
			file.createNewFile();
		try {
			DataOutput out = new DataOutputStream(new FileOutputStream(file));
			localCache.write(out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	private void cleanUp(){
		if(fileDir.exists()){
			File[] files = fileDir.listFiles();
			for(File f : files){
				f.delete();
			}
			fileDir.delete();
		}
	}
}
