package edu.upenn.cis455.job;

import java.util.ArrayList;

import org.apache.hadoop.fs.Path; 
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import edu.upenn.cis455.storage.S3Storage;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class CrawlerMain {
	
	public static String awsAccessKey;
	public static String awsSecretKey;
	public static final String urlsBucket = "search-urls";
	public static final String docsBucket = "search-documents/";
	private static final String s3Location = "s3n://";
	
	/**
	 * @param args
	 * Starts the map reduce crawler by taking in a few arguments like
	 * accessKey, secretKey, inputFile, outputFile, iterations
	 */
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		CrawlerMain.awsAccessKey = args[0];
		CrawlerMain.awsSecretKey = args[1];
		String inputFolder = args[2];
		String outputFile = "s3n://search-urls/" + args[3];
		int iterations = new Integer(args[4]);
		
		//Default separator is tab space
		Configuration conf = new Configuration();
		//mapreduce.input.keyvaluelinerecordreader.key.value.separator
		
		S3Storage s3 = new S3Storage(CrawlerMain.awsAccessKey, CrawlerMain.awsSecretKey);
		//Creating a job
		String outPath =  "";
		for (int i = 0; i<iterations; ++i){
			try{
				outPath = outputFile + i;
		        Job job = new Job(conf, "Distributed Crawler");
				conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", "\t");
				conf.set("localCache", "localCacheMapreduce");
				job.setJarByClass(CrawlerMain.class);
				job.setOutputKeyClass(Text.class);
				job.setOutputValueClass(Text.class); 
				job.setMapperClass(CrawlerMapper.class);
				job.setReducerClass(CrawlerReducer.class); 
				job.setInputFormatClass(KeyValueTextInputFormat.class);
				if(inputFolder.startsWith("s3n://")){
					String[] tokens = inputFolder.split("/");
					inputFolder = tokens[tokens.length - 1];
				}
				ArrayList<String> list = s3.getFilesInFolder(CrawlerMain.urlsBucket, inputFolder);
				if(list.size() > 0){
					for(String file : list){
						if(!file.equals("seed/")){
							FileInputFormat.addInputPath(job, new Path(CrawlerMain.s3Location + CrawlerMain.urlsBucket + "/" +file));
						}
					}
				}
				FileOutputFormat.setOutputPath(job, new Path(outPath));  
				job.waitForCompletion(true);
			    inputFolder = outPath;
			}
			catch(Exception e){
				System.err.println("Error in crawling for iteration: " + i);
				e.printStackTrace();
			}
		}
	}
}
