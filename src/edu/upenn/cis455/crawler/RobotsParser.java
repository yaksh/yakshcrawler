package edu.upenn.cis455.crawler;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;

import edu.upenn.cis445.models.Response;
import edu.upenn.cis445.models.UrlInfo;

/**
 * @author cis455
 * Parses and stores the contents of the robots.txt file in memory
 */
public class RobotsParser {
	public HashMap<String, RobotsTxtInfo> robotsMap;
	private BloomFilter errorCache = new BloomFilter(200000,3,Hash.MURMUR_HASH);
	private String[] userAgentsToCheck = {"*", "cis455crawler"};
	
	/**
	 * Constructor
	 */
	public RobotsParser() {
		this.robotsMap = new HashMap<String, RobotsTxtInfo>();
	}
	
	/**
	 * @param args
	 * For testing purpose
	 * http://crawltest.cis.upenn.edu/nytimes/
	 */
	public static void main(String[] args) {

		RobotsParser r = new RobotsParser();
		System.out.println("Result: " + r.canICrawl("http://www.dmoz.org"));
		for(String host : r.robotsMap.keySet()){
			r.robotsMap.get(host).print();
		}
	}
	
	/**
	 * @param url
	 * Fetched the robots.txt file for a given url
	 */
	public void getFile(String url){
		ClientRequest req = new ClientRequest();
		RobotsTxtInfo robots = new RobotsTxtInfo();
		UrlInfo urlInfo = req.getUrlInfo(url);
		String host = urlInfo.getHost();
		String reqUrl = urlInfo.getFileName();
		String robotFile = "";
		if(reqUrl.equals("/")){
			robotFile = url + "robots.txt";
		}
		else if(reqUrl.equals("")){
			robotFile = url + "/robots.txt";
		}
		else{
			robotFile = url.replace(reqUrl, "/robots.txt");
		}
		try{
			if(!host.isEmpty() || host != null){
				Response res = req.sendGet(robotFile);
				if(res.getResponseCode() == 200){
					if(!res.getContent().isEmpty() && res.getContent() != null){
						String[] lines = res.getContent().split("\n");
						String currentUserAgent = "";
						String crawlDelay = null;
						for(String line : lines){
							if(line.startsWith("#") || line.isEmpty() || line == null)
								continue;
							else if(line.startsWith("User-agent")){
								currentUserAgent = line.split(":")[1].trim();
								robots.addUserAgent(currentUserAgent);
							}
							else if(line.startsWith("Crawl-delay")){
								if(currentUserAgent.equals("cis455crawler")){
									crawlDelay = line.split(":")[1].trim();
								
									if(crawlDelay.contains(".")){
										robots.addCrawlDelay(currentUserAgent, (int)Float.parseFloat(crawlDelay));
									}
									else{
										robots.addCrawlDelay(currentUserAgent, Integer.parseInt(crawlDelay));
									}
								}
								else if(currentUserAgent.equals("*")){
									if(crawlDelay == null){
										crawlDelay = line.split(":")[1].trim();
										robots.addCrawlDelay(currentUserAgent, Integer.parseInt(crawlDelay));
									}
								}
							}
							else{
								if(line.startsWith("Disallow")){
									String[] tokens = line.split(":");
									if(tokens.length == 2)
										robots.addDisallowedLink(currentUserAgent, line.split(":")[1].trim());
								}
								else if(line.startsWith("Allow")){
									robots.addAllowedLink(currentUserAgent, reqUrl);
								}
							}
						}
						robotsMap.put(host, robots);
					}
				}
			}
		}
		catch(Exception e){
			System.err.println("Error parsing robot.txt for: " + urlInfo.getHost());
			errorCache.add(new Key(urlInfo.getHost().getBytes()));
		}
	}
	
	/**
	 * @param url
	 * @return
	 * Returns whether we can go ahead and crawl the given url
	 */
	public boolean canICrawl(String url){
		ClientRequest req = new ClientRequest();
		UrlInfo info = req.getUrlInfo(url);
		String hostname = info.getHost();
		String req_url = info.getPath();
		if(errorCache.membershipTest(new Key(hostname.getBytes())))
			return true;
		if(!robotsMap.containsKey(hostname)){
			getFile(url);
			if(errorCache.membershipTest(new Key(hostname.getBytes())))
				return true;
		}
		return isAllowedToCrawl(req_url, hostname);
	}
	
	/**
	 * @param req
	 * @param host
	 * @return
	 * Checks if the given request url is among the black list urls
	 */
	public boolean isAllowedToCrawl(String req, String host){
		boolean result = true;
		if(robotsMap.containsKey(host)){
			RobotsTxtInfo robots = robotsMap.get(host);
			for(String userAgent : userAgentsToCheck){
				ArrayList<String> disallowedURLS = robots.getDisallowedLinks(userAgent);
				if(disallowedURLS != null && disallowedURLS.size() > 0){
					for(String blackListed : disallowedURLS){
						if(blackListed.startsWith(req)){
							result = false;
							break;
						}
					}
				}
				else
					result = true;
			}
		}
		return result;
	}
	
	/**
	 * @param host
	 * @return
	 * Gives the crawl delay for the given host url
	 */
	public Integer getCrawlDelay(String host){
		Integer result = null;
		if(robotsMap.containsKey(host)){
			for(String userAgent : userAgentsToCheck){
				if(userAgent.equals("cis455crawler")){
					result = robotsMap.get(host).getCrawlDelay(userAgent);
				}
				else if(userAgent.equals("*")){
					if(result == null){
						result = robotsMap.get(host).getCrawlDelay(userAgent);
					}
				}
			}
		}
		return result;
	}
}
