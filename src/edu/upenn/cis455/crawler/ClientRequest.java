package edu.upenn.cis455.crawler;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis445.models.Response;
import edu.upenn.cis445.models.UrlInfo;

/**
 * @author cis455
 * used for making GET and POST requests to a given host
 */
public class ClientRequest {
	
	private final String USER_AGENT = "cis455crawler";
	
	public ClientRequest() {
		System.setProperty("http.maxRedirects", "3");
	}

	public static void main(String[] args) throws Exception {

		ClientRequest http = new ClientRequest();

		System.out.println(http.sendGet("https://www.edx.org/").getContent());
//		System.out.println(http.sendHead("http://www.indeed.com/", 0).getContentLength());
	}

	/**
	 * @param url
	 * @return
	 * @throws Exception
	 * HTTP GET request
	 */
	public Response sendGet(String url){
		HttpURLConnection con = null;
		Response response = new Response();
		BufferedReader in = null;				
		try{
			CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setConnectTimeout(10000);
			
			response = new Response();

			int responseCode = con.getResponseCode();
			if(responseCode == 200){
				response.setResponseCode(responseCode);
				response.setContentLength(con.getContentLength());
				response.setContentType(con.getContentType());
				response.setEncoding(con.getContentEncoding());
				
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response1 = new StringBuffer();
				if(response.getContentLength() <= CrawlerWorker.pageSizeLimit){
					long start = System.currentTimeMillis();
					while ((inputLine = in.readLine()) != null) {
						response1.append(inputLine + "\n");
						if((System.currentTimeMillis() - start) > 3000)
							break;
					}
					response.setContent(response1.toString());
				}
				else{
					response.setContent("");
				}
			}
			else if(responseCode == 301 || responseCode == 302){
				String redirect = con.getRequestProperty("Location");
				if(redirect != null)
					sendGet(redirect);
			}
		}
		catch(Exception e){
			
		}
		finally{
				try {
					if(in != null)
						in.close();
					if(con != null)
						con.disconnect();
				} catch (IOException e) {
				}
		}
		return response;
	}
	
	public Response sendHead(String url, long lastCrawled){
		Response response = null;
		try{
			System.setProperty("http.maxRedirects", "3");
			response = new Response();
			response.setContent("");
			CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

			// optional default is GET
			con.setRequestMethod("HEAD");
			con.setRequestProperty("User-Agent", USER_AGENT);
			if(lastCrawled != 0){
				con.setRequestProperty("If-modified-since", sdf.format(lastCrawled));
			}
			int responseCode = con.getResponseCode();
			response.setResponseCode(responseCode);
			response.setContentLength(con.getContentLength());
			response.setContentType(con.getContentType());
			response.setEncoding(con.getContentEncoding());
		}
		catch(Exception e){
			e.getMessage();
		}
		return response;
	}
	
	/**
	 * @param url
	 * @param params
	 * @return
	 * @throws Exception
	 * HTTP POST request
	 */
	public Response sendPost(String url, String params) throws Exception {
		System.setProperty("http.maxRedirects", "3");
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = params;
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		
		Response response = new Response();

		int responseCode = con.getResponseCode();
		response.setResponseCode(responseCode);
		response.setContentLength(con.getContentLength());
		response.setContentType(con.getContentType());
		response.setEncoding(con.getContentEncoding());

		if(responseCode == 200){
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response1 = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			in.close();
			response.setContent(response1.toString());
		}
		else if(responseCode == 301 || responseCode == 302){
			String redirect = con.getRequestProperty("Location");
			
		}
		
		return response;
	}
	
	public static UrlInfo getUrlInfo(String url){
		UrlInfo info = null;
		try {
			info = new UrlInfo();
			URL urlInfo = new URL(url);
			info.setProtocol(urlInfo.getProtocol());
			info.setHost(urlInfo.getHost());
			info.setHostAndPort(urlInfo.getAuthority());
			info.setQuery(urlInfo.getQuery());
			info.setFileName(urlInfo.getFile());
			info.setPath(urlInfo.getPath());			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return info;
	}
}
