package edu.upenn.cis455.crawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.google.common.collect.Lists;

import edu.upenn.cis445.models.FrontierList;
import edu.upenn.cis445.models.Node;
import edu.upenn.cis445.models.PageDigest;
import edu.upenn.cis445.models.PageGraph;
import edu.upenn.cis445.models.Response;
import edu.upenn.cis445.models.SpreadFrontierList;
import edu.upenn.cis445.models.UrlInfo;
import edu.upenn.cis445.models.UrlResponse;
import edu.upenn.cis455.job.CrawlerMain;
import edu.upenn.cis455.storage.DynamoStorage;
import edu.upenn.cis455.storage.S3Storage;

public class CrawlerWorker implements Runnable{
	
	
	private RobotsParser parsedRobots = new RobotsParser();
	private List<String> links = new ArrayList<>();
	public static long pageSizeLimit = 2 * 1024 * 1024;
	private List<PageGraph> linkGraph = new ArrayList<>();
	private List<PageDigest> storeVisitedUrls = new ArrayList<>();
	private List<File> filesToUpload = new ArrayList<>();
	private File fileDir;
	private String docBucket = "search-documents-2";
	private String rankBucket = "yaksh-pagerank";
	private static String startPoint = "";
	private DynamoStorage db = null;
	private S3Storage s3 = null;
	private int fileBatchSize = 0;
	private ArrayList<UrlResponse> batchedFiles = new ArrayList<>();
	private int crawlCount = 0;
	List<File> linkFile = new ArrayList<>();
	private int threadId;
	
//	public static void main(String[] args) {
//		CrawlerTest crawler = new CrawlerTest();
//		//https://www.reddit.com/,http://www.cnn.com/,https://dbappserv.cis.upenn.edu/crawltest.html,http://stackoverflow.com/questions/2041778/how-to-initialize-hashset-values-by-construction
//		crawler.startCrawl();
//	}
	
	public CrawlerWorker(int threadId) {
		this.threadId = threadId;
	}
	
	@Override
	public void run() {
		startCrawl();
	}
	
	public void startCrawl(){
		try{
			db = new DynamoStorage(Crawler.awsAccessKey, Crawler.awsSecretKey);
			ClientRequest req = new ClientRequest();
			s3 = new S3Storage(Crawler.awsAccessKey, Crawler.awsSecretKey);
			System.out.println("***Worker-"+threadId+" Crawling started***");
			String url = "";
			int skippedUrls = 0;
			long startTime = System.currentTimeMillis();
			long prevCrawlCount = 0;
			while(crawlCount <= Crawler.numUrlsToCrawl){
				try{
					//save state every 5000 urls
					if((crawlCount % 500) == 499 && crawlCount > prevCrawlCount){
						System.out.println("***Saving state at : " + crawlCount + "***");
						saveState();
						prevCrawlCount = crawlCount;
					}
					if(Crawler.URLFrontier.size() == 0){
						break;
					}
					Node node = getLinkUrlFrontier();
					url = node.getUrl();
					if(node == null)
						break;
					if(isUrlVisited(node.getUrl())){
//						System.out.println("Crawler visited url " + node.getUrl());
						skippedUrls++;
						continue;
					}
					Crawler.addToLocalCache(node.getUrl());
					if(!allowedToCrawl(node.getUrl())){
//						System.out.println("Not allowed to crawl (robots.txt)" + node.getUrl());
						skippedUrls++;
						continue;
					}
					long lastCrawled = 0;
//					System.out.println("Crawling: " + node.getUrl());
//					Response response = req.sendHead(node.getUrl(), lastCrawled);
					//canFetchBody(response, node.getUrl())
					if(true){
						crawlCount++;
						Crawler.incrementGlobalCount();
						Response res = req.sendGet(node.getUrl());
						if(res != null){
							if(canFetchBody(res, node.getUrl())){
								processNewDoc(res, node);
								queueFilesUpload(node, res);
							}
						}
					}
					if((crawlCount % 500) == 0 && crawlCount != 0){
						long time = ((System.currentTimeMillis() - startTime) / 1000) ;
						startTime = System.currentTimeMillis();
						System.out.println("***Worker-"+threadId+" Cralwed Urls - " + crawlCount 
								+ " Skipped Urls - " + skippedUrls 
								+ " Time: " + time 
								+ " Frontier size: " + Crawler.URLFrontier.size() + "***");
					}
				}
				catch(Exception e){
					crawlCount--;
					System.err.println("Worker-"+threadId+" Error crawling url : " + url);
				}
			}
			System.out.println("*********Worker-"+threadId+" Finished Crawling (Map phase) Crawled pages = " + crawlCount);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * @return
	 * Fetch links from the queue of links that are getting appended
	 */
	private Node getLinkUrlFrontier(){
		if(Crawler.URLFrontier.size() > 0 ){
			return Crawler.URLFrontier.remove();
		}
		return null;
	}
	
	/**
	 * @param url
	 * @return
	 * Decides whether we can proceed with the given url based 
	 * on the robot.txt
	 */
	public boolean allowedToCrawl(String url){
		try{
//			return parsedRobots.canICrawl(url);
			if(url.contains("javascript:void()"))
				return false;
		}
		catch(Exception e){
			return false;
		}
		return true;
	}
	
	/**
	 * @param url
	 * @return
	 * Checks to see whether the given url was
	 * crawled previosly or not
	 */
	private boolean isUrlVisited(String url){
		if(url != null){
			return Crawler.isVisited(url);
		}
		return false;
	}
	
	private boolean canFetchBody(Response res, String url){
		boolean result = true;
		try{
			if(res.getResponseCode() == 200){
				if(res.getContentLength() > pageSizeLimit){
					result = false;
//					System.out.println("Worker- "+threadId+" No crawl content limit restrictions for " + url);
				}
				else
					result = true;
			}
			else{
				if(res.getResponseCode() == 304){
//					System.out.println("Url " + url + " unmodified since last crawl");
				}
				result = false;
			}
		}
		catch(Exception e){
			result = false;
		}		
		return result;
	}
	
	/**
	 * @param req
	 * Processes the new document to parse the content type
	 * time and body of the new document
	 */
	private void processNewDoc(Response res, Node node){
		if(node.getDepth() == 1){
			PageDigest page = new PageDigest();
			page.setUrl(node.getUrl());
			page.setId(UUID.randomUUID().toString());
			page.setContentType(res.getContentType());
			page.setLastCrawled(System.currentTimeMillis());
			storeVisitedUrls.add(page);
		}
		if(res.getContentLength() > 0 && !res.getContentType().equals("") && res.getContentType().contains("html")){
			Document d = null;
			try{
				if(!res.getContent().isEmpty()){
					d = Jsoup.parse(res.getContent());
				}
			}
			catch(Exception e){
				System.err.println("Error parsing html body");
				e.printStackTrace();
				return;
			}
			if(d != null){
				fetchLinksFromDoc(d, node);
			}
		}
	}
	
	/**
	 * @param doc
	 * @param url
	 * Fetch all the links fron a given url
	 */
	private void fetchLinksFromDoc(Document doc, Node node){
		Elements links = doc.getElementsByTag("a");
		Elements links1 = doc.getElementsByTag("link");
		if(links1.size() > 0)
			links.addAll(links1);
		String[] linkStore = new String[links.size()];
		int count  = 0;
		for (int i = 0; i < links.size(); i++) {
			String href = links.get(i).attr("href");
			if(href != null && !href.equals("")){
				count++;
				String l = getAbsoluteURL(node.getUrl(), href);
				linkStore[i] = l;
				// && allowedToCrawl(l)
				if (!isUrlVisited(l)){
					Node n = new Node(l, (node.getDepth() + 1));
					Crawler.URLFrontier.add(n);
				}
			}
		}
		if(linkStore.length > 0){
			PageGraph linkGraphNode = new PageGraph();
			linkGraphNode.setId(UUID.randomUUID().toString());
			linkGraphNode.setUrl(node.getUrl());
			linkGraphNode.setNumOutboundLinks(count);
			Set<String> s = linkGraphNode.getOutboundLinks();
			if(s == null){
				s = new HashSet();
			}
			for(String link : linkStore){
				s.add(link);
			}
			linkGraphNode.setOutboundLinks(s);
			linkGraph.add(linkGraphNode);
		}
	}
	
	public String getAbsoluteURL(String url, String link) {
		String absUrl = url;
		UrlInfo info = ClientRequest.getUrlInfo(url);
		String pro = info.getProtocol();
		String host = info.getHostAndPort();
		String result = "";
		if(link.startsWith("http://") || link.startsWith("https://")){
			result = link;
		}
		else{
			if(link.startsWith("//")){
				link = link.substring(1);
			}
			if(!link.startsWith("/")){
				link = "/" + link;
			}
			result = pro + "://" + host + link;
		}
		return result;
	}
	
	private void queueFilesUpload(Node node, Response res){
//		System.out.println(node.getUrl());
		Writer writer = null;
		try{
			fileDir = new File("TempStore");
			if(!fileDir.exists()){
				fileDir.mkdir();
			}
			if((crawlCount % 100) == 99 && (crawlCount != 0)){
				File f1 = File.createTempFile("documents", ".txt", fileDir);
				writer = new OutputStreamWriter(new FileOutputStream(f1));
				for(UrlResponse response : batchedFiles){
					writer.write(response.getUrl() + "\t");
			        writer.write(response.getContent());
			        writer.write("\n");
				}
				filesToUpload.add(f1);
				batchedFiles.clear();
			}
			else{
				if(res != null && !res.getContent().equals("")){
					String documentString = Jsoup.parse(res.getContent()).text();
					UrlResponse response = new UrlResponse(node.getUrl(), 
							documentString.replaceAll("\n", " ")
							.replaceAll(" +", " ")
							.replaceAll("\t", " "));
					batchedFiles.add(response);
				}
				else{
					UrlInfo info = ClientRequest.getUrlInfo(node.getUrl());
					UrlResponse response = new UrlResponse(node.getUrl(), 
							info.getFileName().replaceAll("/+", " ")
							.replaceAll("-", " ")
							.replaceAll("_", " "));
					batchedFiles.add(response);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if(writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void cleanUp(){
		if(fileDir.exists()){
			File[] files = fileDir.listFiles();
			for(File f : files){
				f.delete();
			}
			fileDir.delete();
		}
	}
	
	public void saveState(){
		try{
			//Save pages info in db
//			System.out.println("Saving digest-pages");
//			for (List<PageDigest> pages : Lists.partition(storeVisitedUrls, 100)) {
//				try{
//					db.storeUrls(pages);
//				}
//				catch(Exception e){
//					e.printStackTrace();
//				}
//				
//			}
			storeVisitedUrls.clear();
			System.out.println("***Worker-"+threadId+" Storing page-graphs***");
			//Store link graph
//			for (List<PageGraph> links : Lists.partition(linkGraph, 10)) {
//				try{
//					db.storeGraph(links);
//					
//				}
//				catch(Exception e){
//					e.printStackTrace();
//				}
//			}
			for (List<PageGraph> links : Lists.partition(linkGraph, 100)) {
				try{
					File pageRank = File.createTempFile("pageRank", ".txt",fileDir);
					UrlInfo info = new UrlInfo();
					BufferedWriter out = new BufferedWriter(new FileWriter(pageRank,true));
					try{
						for(PageGraph link : links){
							String line = "";
							info = ClientRequest.getUrlInfo(link.getUrl());
							line += info.getProtocol() + "://" + info.getHostAndPort() + info.getPath() + "\t";
							for(String url : link.getOutboundLinks()){
								line += url + ";;";
							}
//							System.out.println(line);
							out.write(line.substring(0, line.length() - 1) + "\n");;
						}
						linkFile.add(pageRank);
					}
					catch(Exception e){
						
					}
					finally{
						out.close();
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			MultipleFileUpload upload = S3Storage.transferManager.uploadFileList(rankBucket, 
					"", fileDir,linkFile);
			linkFile.clear();
			upload.waitForCompletion();
			linkGraph.clear();

			//Store the documents
			System.out.println("***Worker-"+threadId+" Uploading documents***");
			MultipleFileUpload upload1 = S3Storage.transferManager.uploadFileList(docBucket, "", fileDir,
							filesToUpload);
			filesToUpload.clear();
			upload1.waitForCompletion();
		}
		catch(Exception e){
			System.out.println("Worker-Error saving state");
			e.printStackTrace();
		}
	}
}