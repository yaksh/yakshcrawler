package edu.upenn.cis455.crawler;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;

import edu.upenn.cis445.models.FrontierList;
import edu.upenn.cis445.models.Node;
import edu.upenn.cis455.storage.DynamoStorage;
import edu.upenn.cis455.storage.S3Storage;

public class Crawler extends Thread {
	
	public static String awsAccessKey = "";
	public static String awsSecretKey = "";
	public static long numUrlsToCrawl = 0;
	public static String seedPath = "seed.txt";
	public static FrontierList<Node> URLFrontier = new FrontierList<>();
	public static BloomFilter localCache = new BloomFilter();
	private DynamoStorage db = null;
	private S3Storage s3 = null;
	private static int numThreads = 5;
	private static long globalCount = 0;
	
	public static void main(String[] args) {
		Thread[] threads;
		awsAccessKey = args[0];
		awsSecretKey = args[1];
		seedPath = args[2];
		numUrlsToCrawl = Integer.parseInt(args[3]);
		numThreads = Integer.parseInt(args[4]);
		Crawler crawler = new Crawler();
		threads = new Thread[numThreads];
		crawler.setDaemon(true);
		crawler.frontierSetup();
		crawler.setName("Master");
		for(int i = 0; i < numThreads; i++){
			CrawlerWorker crawl = new CrawlerWorker(i);
			Thread t = new Thread(crawl);
			threads[i] = t;
			t.start();
		}
		long start = System.nanoTime();
		long prevCrawlCount = 0;
		long prevSaveStateCount = 0;
		while(true){
			if(crawler.getGlobalCount() > (numUrlsToCrawl * numThreads))
				break;
			if(((crawler.getGlobalCount() % 500) == 499) && globalCount != 0 && crawler.getGlobalCount() > prevCrawlCount){
				System.out.println("---Master- URL crawled "+ crawler.getGlobalCount() 
						+ " Frontier size:  " + URLFrontier.size() 
						+ " Time(ms): " + (System.nanoTime() - start) / 1000000);
				start = System.nanoTime();
				prevCrawlCount = crawler.getGlobalCount();
			}
			if(((crawler.getGlobalCount() % 1500) == 1499) && globalCount != 0 && crawler.getGlobalCount() > prevSaveStateCount){
				crawler.saveState();
				System.out.println("---Master- Saved localCache and URLFrontier");
				prevSaveStateCount = crawler.getGlobalCount();
			}
		}
		for(int i = 0; i < threads.length; i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				System.exit(-1);
				e.printStackTrace();
			}
		}
		System.out.println("---Master- Finished crawling master exiting");
		System.exit(0);
	}
	
	private void frontierSetup(){
		BufferedReader reader = null;
		try{
			db = new DynamoStorage(Crawler.awsAccessKey, Crawler.awsSecretKey);
			ClientRequest req = new ClientRequest();
			s3 = new S3Storage(Crawler.awsAccessKey, Crawler.awsSecretKey);
			System.out.println("***Retrieving url frontier***");
			File frontierFile = File.createTempFile("Frontier", ".txt");
			frontierFile.deleteOnExit();
			s3.download("search-urls", "frontier/urlFrontier.txt", frontierFile);
			reader = new BufferedReader(new FileReader(frontierFile));
			String urls = "";
			String line = "";
			while((line = reader.readLine()) != null){
				if(line.length() > 0){
					Node n = new Node(line, 1);
					URLFrontier.add(n);
				}
			}
			reader.close();
			System.out.println("***Retrieving local cache***");
			File cacheFile = File.createTempFile("Frontier", ".txt");
			cacheFile.deleteOnExit();
			s3.download("yaksh-caching", "localCache", cacheFile);
			FileInputStream input = new FileInputStream(cacheFile);
			if(input.available() > 0){
				DataInput in = new DataInputStream(input);
				localCache.readFields(in);
			}
			else{
				localCache = new BloomFilter(200000,3,Hash.MURMUR_HASH);
			}
			input.close();
			if(URLFrontier.size() == 0){
				System.out.println("***Retrieving seed urls***");
				File seedFile = File.createTempFile("seed", ".txt");
				seedFile.deleteOnExit();
				s3.download("search-urls", "seed/" + Crawler.seedPath, seedFile);
				reader = new BufferedReader(new FileReader(seedFile));
				line = "";
				while((line = reader.readLine()) != null){
					Node n = new Node(line, 1);
					URLFrontier.add(n);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try {
				if(reader != null)
					reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void saveState(){
		try{
			//Save the url frontier
			System.out.println("Saving frontier");
			File urlFrontierFile = Crawler.URLFrontier.writeToFile("URLFrontier/frontier", "txt");
			s3.upload("search-urls", "frontier/urlFrontier.txt", urlFrontierFile);
			
			//Store local cache
			System.out.println("Storing local cache");
			s3.upload("yaksh-caching", "localCache", saveCache());
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static synchronized void addToLocalCache(String url){
		if(url != null && url.length() > 0)
			Crawler.localCache.add(new Key(url.getBytes()));
	}
	
	private File saveCache() throws IOException{
		File file = new File("TempStore/caching");
		file.deleteOnExit();
		if(!file.exists())
			file.createNewFile();
		try {
			DataOutput out = new DataOutputStream(new FileOutputStream(file));
			Crawler.localCache.write(out);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	public static synchronized boolean isVisited(String url){
		return Crawler.localCache.membershipTest(new Key(url.getBytes()));
	}
	
	public static synchronized void incrementGlobalCount(){
		globalCount++;
	}
	
	public synchronized long getGlobalCount(){
		return globalCount;
	}
}