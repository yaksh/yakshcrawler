package edu.upenn.cis455.storage;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.ConsistentReads;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.document.BatchGetItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.KeysAndAttributes;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.util.Tables;

import edu.upenn.cis445.models.PageDigest;
import edu.upenn.cis445.models.PageGraph;

public class DynamoStorage {
	
	private AWSCredentials credentials = null;
	private static AmazonDynamoDBClient dynamoDB = null;
	private Regions defaultRegion = Regions.US_EAST_1;
	public static DynamoDBMapper dbWrapper = null;
	public static DynamoDB dynamo = null;
	
	public DynamoStorage(String accessKey, String secretKey) {
		this.credentials = new BasicAWSCredentials(accessKey, secretKey);
		setupDynamoClient();
		setupDBMapper();
	}
	
	public DynamoStorage() {
		setupCredentials();
		setupDynamoClient();
		setupDBMapper();
	}
	
	private void setupCredentials(){
		try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (/home/cis455/.aws/credentials), and is in valid format.",
                    e);
        }
	}
	
	public void setupDynamoClient(){
		if(credentials != null){
			try{
				dynamoDB = new AmazonDynamoDBClient(credentials);
		        Region usEast1 = Region.getRegion(defaultRegion);
		        dynamoDB.setRegion(usEast1);
		        dynamo = new DynamoDB(dynamoDB);
			}
			catch(Exception e){
				System.out.println("Error in getting the dynamo client");
				e.printStackTrace();
			}
		}
	}
	
	public void setupDBMapper(){
		if(dynamoDB != null){
			dbWrapper = new DynamoDBMapper(dynamoDB);
		}
	}
	
	
    private static void init() throws Exception {
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
        dynamoDB = new AmazonDynamoDBClient(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        dynamoDB.setRegion(usWest2);
    }
    
    public void storeUrls(List<PageDigest> pages){
    	try{
    		DynamoStorage.dbWrapper.batchSave(pages);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    public void storeGraph(List<PageGraph> list){
    	try{
    		DynamoStorage.dbWrapper.batchSave(list);
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    public List<Item> getUrls(String table, String hashKey, List<String> value){
    	List<Item> result = null;
    	try{
    		TableKeysAndAttributes condition = new TableKeysAndAttributes(table);
    		for(String i : value){
    			condition.addHashOnlyPrimaryKey(hashKey, i);
    		}
    		BatchGetItemOutcome r = DynamoStorage.dynamo.batchGetItem(condition);
    		for(String t : r.getTableItems().keySet()){
    			result = r.getTableItems().get(t);
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	return result;
    }

    public static void main(String[] args) throws Exception {
    	DynamoStorage dynamo = new DynamoStorage();
    	dynamo.setupDBMapper();
    	TableKeysAndAttributes pages = new TableKeysAndAttributes("page-digest");
    	pages.addHashOnlyPrimaryKey("url", "acw.com");
    	pages.addHashOnlyPrimaryKey("url", "iyu.com");
    	BatchGetItemOutcome result =DynamoStorage.dynamo.batchGetItem(pages);
    	
    	for(String e : result.getTableItems().keySet()){
    		System.out.println("Tablename: " + e);
    		List<Item> items = result.getTableItems().get(e);
    	    for (Item item : items) {
    	        System.out.println(item.get("id"));
    	    }
    	}
//      init();
//        try {
//            String tableName = "my-favorite-movies-table";
//
//            // Create table if it does not exist yet
//            if (Tables.doesTableExist(dynamoDB, tableName)) {
//                System.out.println("Table " + tableName + " is already ACTIVE");
//            } else {
//                // Create a table with a primary hash key named 'name', which holds a string
//                CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(tableName)
//                    .withKeySchema(new KeySchemaElement().withAttributeName("name").withKeyType(KeyType.HASH))
//                    .withAttributeDefinitions(new AttributeDefinition().withAttributeName("name").withAttributeType(ScalarAttributeType.S))
//                    .withProvisionedThroughput(new ProvisionedThroughput().withReadCapacityUnits(1L).withWriteCapacityUnits(1L));
//                    TableDescription createdTableDescription = dynamoDB.createTable(createTableRequest).getTableDescription();
//                System.out.println("Created Table: " + createdTableDescription);
//
//                // Wait for it to become active
//                System.out.println("Waiting for " + tableName + " to become ACTIVE...");
//                Tables.awaitTableToBecomeActive(dynamoDB, tableName);
//            }
//
//            // Describe our new table
//            DescribeTableRequest describeTableRequest = new DescribeTableRequest().withTableName(tableName);
//            TableDescription tableDescription = dynamoDB.describeTable(describeTableRequest).getTable();
//            System.out.println("Table Description: " + tableDescription);
//
//            // Add an item
//            Map<String, AttributeValue> item = newItem("Bill & Ted's Excellent Adventure", 1989, "****", "James", "Sara");
//            PutItemRequest putItemRequest = new PutItemRequest(tableName, item);
//            PutItemResult putItemResult = dynamoDB.putItem(putItemRequest);
//            System.out.println("Result: " + putItemResult);
//
//            // Add another item
//            item = newItem("Airplane", 1980, "*****", "James", "Billy Bob");
//            putItemRequest = new PutItemRequest(tableName, item);
//            putItemResult = dynamoDB.putItem(putItemRequest);
//            System.out.println("Result: " + putItemResult);
//
//            // Scan items for movies with a year attribute greater than 1985
//            HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();
//            Condition condition = new Condition()
//                .withComparisonOperator(ComparisonOperator.GT.toString())
//                .withAttributeValueList(new AttributeValue().withN("1985"));
//            scanFilter.put("year", condition);
//            ScanRequest scanRequest = new ScanRequest(tableName).withScanFilter(scanFilter);
//            ScanResult scanResult = dynamoDB.scan(scanRequest);
//            System.out.println("Result: " + scanResult);
//
//        } catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to AWS, but was rejected with an error response for some reason.");
//            System.out.println("Error Message:    " + ase.getMessage());
//            System.out.println("HTTP Status Code: " + ase.getStatusCode());
//            System.out.println("AWS Error Code:   " + ase.getErrorCode());
//            System.out.println("Error Type:       " + ase.getErrorType());
//            System.out.println("Request ID:       " + ase.getRequestId());
//        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with AWS, "
//                    + "such as not being able to access the network.");
//            System.out.println("Error Message: " + ace.getMessage());
//        }
    }

    private static Map<String, AttributeValue> newItem(String name, int year, String rating, String... fans) {
        Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
        item.put("name", new AttributeValue(name));
        item.put("year", new AttributeValue().withN(Integer.toString(year)));
        item.put("rating", new AttributeValue(rating));
        item.put("fans", new AttributeValue().withSS(fans));

        return item;
    }
}
