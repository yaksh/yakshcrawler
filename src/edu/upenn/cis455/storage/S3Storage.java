package edu.upenn.cis455.storage;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.FileSystem;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.apache.hadoop.util.hash.Hash;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;

import edu.upenn.cis455.job.CrawlerMain;

public class S3Storage {
	
	private AWSCredentials credentials = null;
	public static AmazonS3 s3 = null;
	public static TransferManager transferManager = null;
	private Regions defaultRegion = Regions.US_EAST_1;
	
	public S3Storage(String accessKey, String secretKey) {
		this.credentials = new BasicAWSCredentials(accessKey, secretKey);
		setupS3Client();
		setupTransferManager();
	}
	
	public S3Storage() {
		setupCredentials();
		setupS3Client();
		setupTransferManager();
	}
	
	private void setupCredentials(){
		try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (/home/cis455/.aws/credentials), and is in valid format.",
                    e);
        }
	}
	
	private void setupS3Client(){
		if(credentials != null){
			try{
				s3 = new AmazonS3Client(credentials);
		        Region usEast1 = Region.getRegion(defaultRegion);
		        s3.setRegion(usEast1);
			}
			catch(Exception e){
				System.out.println("Error in getting the S3 client");
				e.printStackTrace();
			}
		}
	}
	
	private void setupTransferManager(){
		transferManager = new TransferManager(credentials);
	}
	
	public void createBucket(String bucketName){
		if(bucketName != null && !bucketName.equals("")){
			if(s3 != null){
				try{
					s3.createBucket(bucketName);
				}
				catch(Exception e){
					System.out.println("Error in creating bucket: " + bucketName);
					e.printStackTrace();
				}
			}	
		}
	}
	
	public java.util.List<Bucket> getBucketList(){
		java.util.List<Bucket> l = null;
		if(s3 != null){
			l = s3.listBuckets();
		}
		return l;
	}
	
	public void upload(String bucket, String key, File file){
		try{
			s3.putObject(new PutObjectRequest(bucket, key, file));
		}
		catch(Exception e){
			System.out.println("Error in storing the object: " + file.getName());
			e.printStackTrace();
		}
	}
	
	/**
	 * @param bucketName
	 * @param key
	 * @return A inputStream from which the data is read
	 */
	public void download(String bucketName, String key, File file){
		try{
			transferManager = new TransferManager(credentials);
			Download myDownload = transferManager.download(bucketName, key, file);
			myDownload.waitForCompletion();
		}
		catch(Exception e){
			System.out.println("Error in fetching the object: " + key);
			e.printStackTrace();
		}
	}
	
	public java.util.List<S3ObjectSummary> getObjectSummary(String bucketName){
		java.util.List<S3ObjectSummary> l = null;
		try{
			l = s3.listObjects(new ListObjectsRequest()
					.withBucketName(bucketName)).getObjectSummaries();
		}
		catch(Exception e){
			System.out.println("Error in fetching the object summaries for: " + bucketName);
			e.printStackTrace();
		}
		return l;
	}
	
	public void deleteObject(String bucket, String key){
		try{
			s3.deleteObject(bucket, key);
		}
		catch(Exception e){
			System.out.println("Error in deleting object: " + key);
			e.printStackTrace();
		}
	}
	
	public void deleteBucket(String bucket){
		try{
			s3.deleteBucket(bucket);
		}
		catch(Exception e){
			System.out.println("Error in deleting bucket " + bucket);
			e.printStackTrace();
		}
	}
	
	public synchronized String getObjectData(S3Object object){
		BufferedReader reader = new BufferedReader(new InputStreamReader(object.getObjectContent()));
		StringBuffer buffer = new StringBuffer();
		try{
			while (true) {
	            String line = reader.readLine();
	            if (line == null) 
	            	break;
	            buffer.append(line);
	        }
		}
		catch(Exception e){
			System.out.println("Error in downloading data from object: " + object.getKey());
			e.printStackTrace();
		}
		finally{
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return (buffer.length() > 0) ? buffer.toString() : "";
	}
	
	public ArrayList<String> getFilesInFolder(String bucket, String folder){
		ArrayList<String> list = new ArrayList<>();
		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
	        .withBucketName(bucket)
	        .withPrefix(folder + "/");

        ObjectListing objectListing = s3.listObjects(listObjectsRequest);

		for (S3ObjectSummary summary : objectListing.getObjectSummaries()) {
		    list.add(summary.getKey());
		}
		return list;
	}
	
    public static void main(String[] args) throws IOException {

    	S3Storage s3 = new S3Storage();
    	ArrayList<String> list = s3.getFilesInFolder("search-urls", "seed");
	    if(list.size() > 0){
	    	for(String file : list){
		    	System.out.println(file);
		    }
	    }
    	
//    	BloomFilter b1 = new BloomFilter(15000,2,Hash.MURMUR_HASH);
//    	b1.add(new Key("Jatin".getBytes()));
//    	DataOutput out = new DataOutputStream(new FileOutputStream(new File("Jatin")));
//    	DataInput in = new DataInputStream(new FileInputStream(new File("Jatin")));
//    	b1.write(out);
//    	BloomFilter b2 = new BloomFilter();
//    	b2.readFields(in);
//    	System.out.println(b2.membershipTest(new Key("J".getBytes())));

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (/home/cis455/.aws/credentials).
         */
//		AWSCredentials credentials = null;
//        try {
//            credentials = new ProfileCredentialsProvider("default").getCredentials();
//        } catch (Exception e) {
//            throw new AmazonClientException(
//                    "Cannot load the credentials from the credential profiles file. " +
//                    "Please make sure that your credentials file is at the correct " +
//                    "location (/home/cis455/.aws/credentials), and is in valid format.",
//                    e);
//        }
//
//        AmazonS3 s3 = new AmazonS3Client(credentials);
//        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
//        s3.setRegion(usWest2);
//
//        String bucketName = "my-first-s3-bucket-" + UUID.randomUUID();
//        String key = "MyObjectKey";
//
//        System.out.println("===========================================");
//        System.out.println("Getting Started with Amazon S3");
//        System.out.println("===========================================\n");
//
//        try {
//            /*
//             * Create a new S3 bucket - Amazon S3 bucket names are globally unique,
//             * so once a bucket name has been taken by any user, you can't create
//             * another bucket with that same name.
//             *
//             * You can optionally specify a location for your bucket if you want to
//             * keep your data closer to your applications or users.
//             */
//            System.out.println("Creating bucket " + bucketName + "\n");
//            s3.createBucket(bucketName);
//
//            /*
//             * List the buckets in your account
//             */
//            System.out.println("Listing buckets");
//            for (Bucket bucket : s3.listBuckets()) {
//                System.out.println(" - " + bucket.getName());
//            }
//            System.out.println();
//
//            /*
//             * Upload an object to your bucket - You can easily upload a file to
//             * S3, or upload directly an InputStream if you know the length of
//             * the data in the stream. You can also specify your own metadata
//             * when uploading to S3, which allows you set a variety of options
//             * like content-type and content-encoding, plus additional metadata
//             * specific to your applications.
//             */
//            System.out.println("Uploading a new object to S3 from a file\n");
//            s3.putObject(new PutObjectRequest(bucketName, key, createSampleFile()));
//
//            /*
//             * Download an object - When you download an object, you get all of
//             * the object's metadata and a stream from which to read the contents.
//             * It's important to read the contents of the stream as quickly as
//             * possibly since the data is streamed directly from Amazon S3 and your
//             * network connection will remain open until you read all the data or
//             * close the input stream.
//             *
//             * GetObjectRequest also supports several other options, including
//             * conditional downloading of objects based on modification times,
//             * ETags, and selectively downloading a range of an object.
//             */
//            //s3n://common-crawl/crawl-002/2010/01/07/18/1262876244253_18.arc.gz
//            System.out.println("Downloading an object");
//            S3Object object = s3.getObject(new GetObjectRequest("common-crawl/crawl-002/", "2010/01/07/18/1262876244253_18.arc.gz"));
//            System.out.println("Content-Type: "  + object.getObjectMetadata().getContentType());
//            displayTextInputStream(object.getObjectContent());
//
//            /*
//             * List objects in your bucket by prefix - There are many options for
//             * listing the objects in your bucket.  Keep in mind that buckets with
//             * many objects might truncate their results when listing their objects,
//             * so be sure to check if the returned object listing is truncated, and
//             * use the AmazonS3.listNextBatchOfObjects(...) operation to retrieve
//             * additional results.
//             */
//            System.out.println("Listing objects");
//            ObjectListing objectListing = s3.listObjects(new ListObjectsRequest()
//                    .withBucketName(bucketName)
//                    .withPrefix("My"));
//            for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
//                System.out.println(" - " + objectSummary.getKey() + "  " +
//                                   "(size = " + objectSummary.getSize() + ")");
//            }
//            System.out.println();
//
//            /*
//             * Delete an object - Unless versioning has been turned on for your bucket,
//             * there is no way to undelete an object, so use caution when deleting objects.
//             */
//            System.out.println("Deleting an object\n");
//            s3.deleteObject(bucketName, key);
//
//            /*
//             * Delete a bucket - A bucket must be completely empty before it can be
//             * deleted, so remember to delete any objects from your buckets before
//             * you try to delete them.
//             */
//            System.out.println("Deleting bucket " + bucketName + "\n");
//            s3.deleteBucket(bucketName);
//        } catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response for some reason.");
//            System.out.println("Error Message:    " + ase.getMessage());
//            System.out.println("HTTP Status Code: " + ase.getStatusCode());
//            System.out.println("AWS Error Code:   " + ase.getErrorCode());
//            System.out.println("Error Type:       " + ase.getErrorType());
//            System.out.println("Request ID:       " + ase.getRequestId());
//        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with S3, "
//                    + "such as not being able to access the network.");
//            System.out.println("Error Message: " + ace.getMessage());
//        }
    }

    /**
     * Creates a temporary file with text data to demonstrate uploading a file
     * to Amazon S3
     *
     * @return A newly created temporary file with text data.
     *
     * @throws IOException
     */
    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.close();

        return file;
    }

    /**
     * Displays the contents of the specified input stream as text.
     *
     * @param input
     *            The input stream to display as text.
     *
     * @throws IOException
     */
    private static void displayTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;

            System.out.println("    " + line);
        }
        System.out.println();
    }
    
	public File createTempFile(String prefix, String suffix){
	     String tempDir = System.getProperty("java.io.tmpdir");
	     String fileName = (prefix != null ? prefix : "" ) + (suffix != null ? suffix : "" ) ;
	     return new File(tempDir, fileName);
	}
}
